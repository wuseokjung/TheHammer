"""
    The Hammer
    Copyright (C) 2018 JustMaffie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from discord.ext import commands
import discord
from thehammer.decorators import is_server_mod
import datetime
from thehammer.module import Module

now = lambda: datetime.datetime.utcnow()


class Mod(Module):
    def __init__(self, bot):
        self.bot = bot
        self.ban_queue = []
        self.kick_queue = []

    @commands.command()
    @is_server_mod()
    async def kick(self, ctx, user: discord.Member, *, reason: str=None):
        is_special = ctx.author == ctx.guild.owner
        member = ctx.guild.get_member(ctx.bot.user.id)
        kick_members = member.guild_permissions.kick_members
        admin = member.guild_permissions.administrator
        if not kick_members and not admin:
            message = "Hey, I can't do that, "
            "please ask an administrator to give me "
            "KICK_MEMBERS or ADMINISTRATOR permission!"
            return await ctx.send(message)
        if ctx.author.top_role.position > user.top_role.position or is_special:
            guild = await self.bot.modlog.get_guild(ctx.guild)
            self.kick_queue.append((guild.id, user.id))
            _reason = "[{moderator}] {reason}"
            _reason = _reason.format(moderator=str(ctx.author), reason=reason)
            await user.kick(reason=_reason)
            _type = "Kick"
            moderator = ctx.author
            await guild.new_case(_type, user, moderator, reason)
            message = "Kicked user <@{}> for {}!"
            return await ctx.send(message.format(user.id, reason))
        else:
            return await ctx.send("Hey, I'm sorry, "
                                  "but that user is higher in the "
                                  "hierarchy than you are, "
                                  "I can't let you do that...")

    @commands.command()
    @is_server_mod()
    async def ban(self, ctx, user: discord.Member, *, reason: str=None):
        is_special = ctx.author == ctx.guild.owner
        member = ctx.guild.get_member(ctx.bot.user.id)
        ban_members = member.guild_permissions.ban_members
        admin = member.guild_permissions.administrator
        if not ban_members and not admin:
            return await ctx.send("Hey, I can't do that, "
                                  "please ask an administrator to give me "
                                  "BAN_MEMBERS or ADMINISTRATOR permission!")
        if ctx.author.top_role.position > user.top_role.position or is_special:
            guild = await self.bot.modlog.get_guild(ctx.guild)
            self.ban_queue.append((guild.id, user.id))
            _reason = "[{moderator}] {reason}"
            _reason = _reason.format(moderator=str(ctx.author), reason=reason)
            await user.ban(reason=_reason)
            _type = "Ban"
            moderator = ctx.author
            await guild.new_case(_type, user, moderator, reason)
            msg = "Banned user <@{}> for {}!"
            return await ctx.send(msg.format(user.id, reason))
        else:
            return await ctx.send("Hey, I'm sorry, "
                                  "but that user is higher in the hierarchy "
                                  "than you are, I can't let you do that...")

    @commands.command()
    @is_server_mod()
    async def hackban(self, ctx, _id: int, *, reason: str=None):
        bot = self.bot
        user = ctx.guild.get_member(_id)
        if user is not None:
            return await ctx.invoke(self.ban, user=user)
        try:
            guild = await self.bot.modlog.get_guild(ctx.guild)
            user = await bot.get_user_info(_id)
            await bot.http.ban(_id, guild.id, 0)
            _type = "Hackban"
            moderator = ctx.author
            await guild.new_case(_type, user, moderator, reason)
            msg = "Hackbanned user with the id {} for {}!"
            await ctx.send(msg.format(_id, reason))
        except discord.NotFound:
            await ctx.send("I'm sorry, this ID is invalid. "
                           "Can't really ban a person that doesn't exist, "
                           "right?")
        except discord.Forbidden:
            await ctx.send("I'm sorry, "
                           "I can't ban this user, not enough permissions?")

    @commands.command()
    @is_server_mod()
    async def mute(self, ctx, user: discord.Member, *, reason: str=None):
        is_special = ctx.author == ctx.guild.owner
        member = ctx.guild.get_member(ctx.bot.user.id)
        manage_roles = member.guild_permissions.manage_roles
        admin = member.guild_permissions.administrator
        if not manage_roles and not admin:
            return await ctx.send("Hey, "
                                  "I can't do that, please ask "
                                  "an administrator to give me "
                                  "MANAGE_ROLES or ADMINISTRATOR permission!")
        if ctx.author.top_role.position > user.top_role.position or is_special:
            guild = await self.bot.modlog.get_guild(ctx.guild)
            mutedrole = await guild.settings.get("muted_role", None)
            if not mutedrole:
                msg = "There is not currently a muted role set!"
                return await ctx.send(msg)
            roles = guild.guild.roles
            mutedrole = discord.utils.get(roles, id=mutedrole)
            if not mutedrole:
                msg = "The configured muted role is invalid, "
                "has it been deleted?"
                return await ctx.send(msg)
            if mutedrole in user.roles:
                return await ctx.send("This user is already muted!")
            _reason = "[{moderator}] {reason}"
            author = str(ctx.author)
            _reason = _reason.format(moderator=author, reason=reason)
            await user.add_roles(mutedrole, reason=_reason, atomic=True)
            _type = "Mute"
            moderator = ctx.author
            await guild.new_case(_type, user, moderator, reason)
            msg = "Muted user <@{}> for {}!"
            return await ctx.send(msg.format(user.id, reason))
        else:
            return await ctx.send("Hey, I'm sorry, "
                                  "but that user is higher in the "
                                  "hierarchy than you are, "
                                  "I can't let you do that...")

    async def on_member_ban(self, guild, user):
        if (guild.id, user.id) in self.ban_queue:
            self.ban_queue.remove((guild.id, user.id))
            return
        try:
            guild = await self.bot.modlog.get_guild(guild)
            action = discord.AuditLogAction.ban
            _guild = guild.guild
            async for entry in _guild.audit_logs(action=action):
                if not entry.user.id == guild.guild.me.id:
                    if entry.target.id == user.id:
                        type = "Ban"
                        target = entry.target
                        user = entry.user
                        reason = entry.reason
                        return await guild.new_case(type, target, user, reason)
        except discord.Forbidden:
            pass
        except Exception as e:
            self.bot.logger.exception("An error occurred", exc_info=e)

    async def on_member_remove(self, member):
        if (member.guild.id, member.id) in self.kick_queue:
            self.kick_queue.remove((member.guild.id, member.id))
            return
        try:
            guild = await self.bot.modlog.get_guild(member.guild)
            ban = discord.utils.get(await guild.guild.bans(), user=member)
            if ban:
                return
            _guild = guild.guild
            _action = discord.AuditLogAction.ban
            after = (now - datetime.timedelta(seconds=60))
            async for entry in _guild.audit_logs(action=_action, after=after):
                if not entry.user.id == guild.guild.me.id:
                    if entry.target.id == member.id:
                        type = "Kick"
                        target = entry.target
                        user = entry.user
                        reason = entry.reason
                        return await guild.new_case(type, target, user, reason)
        except discord.Forbidden:
            pass
        except Exception as e:
            self.bot.logger.exception("An error occurred", exc_info=e)

    @commands.command()
    @is_server_mod()
    async def unmute(self, ctx, user: discord.Member, *, reason: str=None):
        is_special = ctx.author == ctx.guild.owner
        member = ctx.guild.get_member(ctx.bot.user.id)
        manage_roles = member.guild_permissions.manage_roles
        admin = member.guild_permissions.administrator
        if not manage_roles and not admin:
            return await ctx.send("Hey, "
                                  "I can't do that, please ask "
                                  "an administrator to give me "
                                  "MANAGE_ROLES or ADMINISTRATOR permission!")
        if ctx.author.top_role.position > user.top_role.position or is_special:
            guild = await self.bot.modlog.get_guild(ctx.guild)
            mutedrole = await guild.settings.get("muted_role", None)
            if not mutedrole:
                msg = "There is not currently a muted role set!"
                return await ctx.send(msg)
            mutedrole = discord.utils.get(guild.guild.roles, id=mutedrole)
            if not mutedrole:
                msg = "The configured muted role is invalid, "
                "has it been deleted?"
                return await ctx.send(msg)
            if not mutedrole in user.roles:
                return await ctx.send("This user is not muted!")
            _reason = "[{moderator}] {reason}"
            author = str(ctx.author)
            _reason = _reason.format(moderator=author, reason=reason)
            await user.remove_roles(mutedrole, reason=_reason, atomic=True)
            _type = "Unmute"
            moderator = ctx.author
            await guild.new_case(_type, user, moderator, reason)
            msg = "Unmuted user <@{}> for {}!"
            return await ctx.send(msg.format(user.id, reason))
        else:
            return await ctx.send("Hey, I'm sorry, "
                                  "but that user is higher in "
                                  "the hierarchy than you are, "
                                  "I can't let you do that...")

    @commands.command()
    @is_server_mod()
    async def reason(self, ctx, case: int, *, reason: str):
        guild = await self.bot.modlog.get_guild(ctx.guild)
        case = await guild.get_case(case)
        if not case:
            return await ctx.send("That case doesn't exist!")
        await case.set("reason", reason)
        await case.set("moderator", ctx.author.id)
        await case.save()
        return await ctx.send("Reason Updated!")


def setup(bot):
    bot.load_module(Mod)
