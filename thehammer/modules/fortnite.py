"""
    The Hammer
    Copyright (C) 2018 JustMaffie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import aiohttp
from thehammer.utils import TimerResetDict
import discord
import datetime
from math import ceil
from io import BytesIO
import PIL.Image
import PIL.ImageDraw
import PIL.ImageFont
from imgurpython import ImgurClient
import os


priceEmotes = {
    "vbucks": "<:vbucks:454715357102604298>",
    "vip": "<:vip:454737457167073281>",
    False: ""
}

rarityColors = {
    "common": discord.Colour(0x6e6f72),
    "uncommon": discord.Colour(0x00ac11),
    "rare": discord.Colour(0x0274df),
    "epic": discord.Colour(0x8a12c4),
    "legendary": discord.Colour(0xfb4f04)
}

purple = discord.Colour.purple()

now = lambda: datetime.datetime.utcnow()


def format_date(date):
    return datetime.datetime.strptime(date[:-5], "%Y-%m-%dT%H:%M:%S")


class Urls:
    shop = "https://fnbr.co/api/shop"
    status = "https://api.justmaffie.nl/api/v2/fortnite/status"

    @staticmethod
    def news(gamemode):
        base = "https://api.justmaffie.nl/api/v1/fortnite/news/{}"
        return base.format(gamemode)

    @staticmethod
    def random(type):
        base = "https://fnbr.co/api/random?type={}"
        return base.format(type)

    @staticmethod
    def search(name, type):
        type = "&type={}".format(type) if type else ""
        base = "https://fnbr.co/api/images?search={}{}"
        return base.format(name, type)

    @staticmethod
    def stats(platform, username):
        base = "https://api.fortnitetracker.com/v1/profile/{}/{}"
        return base.format(platform, username)


class FortniteHTTP:
    def __init__(self, bot):
        self.bot = bot
        name = "Fortnite.Caches.StatsCache"
        self.cache = TimerResetDict(bot, name, 10 * 60)
        name = "Fortnite.Caches.DailyShopCache"
        self.daily_image = TimerResetDict(bot, name, 30 * 60)
        imgur = self.bot.config.imgur
        self.imgur = ImgurClient(imgur.client_id, imgur.client_secret)
        self.justmaffie_headers = {
            "X-API-Key": self.bot.config.fortnite.justmaffie
        }

    async def upload_image(self, path, name="file.png"):
        result = self.imgur.upload_from_path(path, {
            "name": name
        })
        return result

    async def get(self, api_url, headers={}):
        async with aiohttp.ClientSession() as session:
            async with session.get(api_url, headers=headers) as resp:
                content = await resp.json()
        return content

    async def post(self, api_url, data={}, headers={}):
        async with aiohttp.ClientSession() as session:
            async with session.post(api_url, data=data,
                                    headers=headers) as resp:
                content = await resp.json()
        return content

    async def get_shop(self):
        headers = {'X-API-Key': self.bot.config.fortnite.fnbr}
        result = await self.get(Urls.shop, headers)
        return result.get('data', {})

    async def get_shop_embed(self):
        result = await self.get_shop()
        daily = result['daily']
        featured = result['featured']
        date = result['date']
        return FortniteShop(daily, featured, date)

    async def get_shop_image(self):
        result = await self.get_shop()
        return FortniteShopImage(self, result)

    async def random_cosmetic(self, type):
        result = await self.get(Urls.random(type))
        return await self.get_cosmetic(result['item']['name'], type)

    async def get_cosmetic(self, name, type=None):
        headers = {'X-API-Key': self.bot.config.fortnite.fnbr}
        url = Urls.search(name, type)
        result = await self.get(url, headers)
        result = result['data']
        if len(result) > 0:
            return FortniteCosmetic(**result[0])
        return EmptyFortniteCosmetic()

    async def get_stats(self, username, platform):
        platform = platform.lower()
        if (platform, username) in self.cache:
            result = self.cache[(platform, username)]
            return result
        else:
            headers = {
                "TRN-Api-Key": self.bot.config.fortnite.fortnitetracker
            }
            url = Urls.stats(platform, username)
            result = await self.get(url, headers=headers)
        if 'error' in result:
            return result['error']
        self.cache[(platform, username)] = result
        return result

    async def get_news(self, gamemode):
        url = Urls.news(gamemode)
        result = await self.get(url, headers=self.justmaffie_headers)
        return FortniteNews(result['messages'], result['date'])

    async def get_status(self):
        result = await self.get(Urls.status, headers=self.justmaffie_headers)
        return result


class FortniteNews:
    def __init__(self, messages, time):
        self.messages = messages
        self.time = time

    async def send(self, ctx):
        for message in self.messages:
            t = format_date(self.time)
            embed = discord.Embed(color=purple, timestamp=t)
            embed.add_field(name=message['title'], value=message['content'])
            embed.set_thumbnail(url=message['image'])
            await ctx.send(embed=embed)


class FortniteShop:
    def __init__(self, daily, featured, time):
        self.daily = daily
        self.featured = featured
        self.time = time

    async def send(self, ctx):
        t = format_date(self.time)
        featured_items = discord.Embed(color=purple, timestamp=t)
        featured_items.set_author(name="Featured Items")
        featured_items.set_footer(text="Powered by fnbr.co")
        for item in self.featured:
            _name = item['name']
            rarity = item['rarity'].capitalize()
            type = item['readableType']
            name = "{name} ({rarity} {type}"
            name = name.format(name=_name, rarity=rarity, type=type)
            value = "{emote} {price}\n"
            emote = priceEmotes['vbucks']
            price = item['price']
            value = value.format(emote=emote, price=price)
            featured_items.add_field(name=name, value=value)
        t = format_date(self.time)
        daily_items = discord.Embed(color=purple, timestamp=t)
        daily_items.set_author(name="Daily Items")
        daily_items.set_footer(text="Powered by fnbr.co")
        for item in self.daily:
            _name = item['name']
            rarity = item['rarity'].capitalize()
            type = item['readableType']
            name = "{name} ({rarity} {type}"
            name = name.format(name=_name, rarity=rarity, type=type)
            value = "{emote} {price}\n"
            emote = priceEmotes['vbucks']
            price = item['price']
            value = value.format(emote=emote, price=price)
            daily_items.add_field(name=name, value=value)
        await ctx.send(embed=featured_items)
        await ctx.send(embed=daily_items)


class FortniteShopImage:
    def __init__(self, http, result):
        self.http = http
        self.daily = result.get('daily', [])
        self.featured = result.get('featured', [])
        self.date = result['date']
        self.rowsize = 4
        self.itemsize = 200
        self.padding = 20
        self.IMAGENAME = 'generatedshopimage.png'

    async def send(self, ctx):
        if "image" in self.http.daily_image:
            image = self.http.daily_image['image']
            t = format_date(self.date)
            embed = discord.Embed(timestamp=t, colour=purple)
            embed.set_author(name="Fortnite Shop")
            embed.set_image(url=image)
            return await ctx.send(embed=embed)
        image = await self.generate()
        image.save("tmp/daily.png", "PNG")
        result = await self.http.upload_image("tmp/daily.png", self.IMAGENAME)
        os.remove("tmp/daily.png")
        self.http.daily_image['image'] = result['link']
        t = format_date(self.date)
        embed = discord.Embed(timestamp=t, colour=purple)
        embed.set_author(name="Fortnite Shop")
        embed.set_image(url=result['link'])
        return await ctx.send(embed=embed)

    async def generate(self):
        items = self.featured + self.daily
        rows = ceil(len(items) / self.rowsize)
        l = (self.rowsize + 1)
        width = round(self.padding * l + self.itemsize * self.rowsize)
        height = round(self.padding * (rows + 1) + self.itemsize * rows)
        background = PIL.Image.new('RGBA', (width, height), (0, 0, 0, 0))
        row_array = await self.split_rows(items, self.rowsize)
        left = self.padding
        top = self.padding
        for row in row_array:
            for item in row:
                url = item.get('images', {}).get('gallery')
                if url:
                    image = await self.retrieve_image(url, self.itemsize)
                    background.paste(image, (left, top), image)
                left += self.itemsize + self.padding
            top += self.itemsize + self.padding
            left = self.padding
        return background

    async def split_rows(self, items, rowsize):
        rows = []
        for i in range(0, len(items), rowsize):
            rows.append(items[i: i + rowsize])
        return rows

    async def retrieve_image(self, url, size):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                bytes = await response.read()
        image = PIL.Image.open(BytesIO(bytes)).convert('RGBA')
        return image.resize((size, size))


class FortniteCosmetic:
    def __init__(self, id, name, price,
                 priceIcon, priceIconLink, images,
                 rarity, type, readableType):
        self.id = id
        self.name = name
        self.price = price
        self.priceIcon = priceIcon
        self.images = images
        self.rarity = rarity
        self.type = type
        self.readableType = readableType

    async def send(self, ctx):
        color = rarityColors[self.rarity]
        embed = discord.Embed(color=color, timestamp=now())
        embed.set_author(name=self.name)
        if self.images['gallery']:
            embed.set_image(url=self.images['gallery'])
        else:
            embed.add_field(name="Name", value=self.name)
            price = "{emote} {price}"
            emote = priceEmotes[self.priceIcon]
            _price = self.price
            price = price.format(emote=emote, price=_price)
            embed.add_field(name="Price", value=price)
            embed.add_field(name="Type", value=self.readableType)
            embed.add_field(name="Rarity", value=self.rarity.capitalize())
            embed.set_thumbnail(url=self.images['icon'])
        embed.set_footer(text="Powered by fnbr.co")
        await ctx.send(embed=embed)


class EmptyFortniteCosmetic(FortniteCosmetic):
    def __init__(self):
        super().__init__(None, None, None, None, None, None, None, None, None)

    async def send(self, ctx):
        await ctx.send("This cosmetic could not be found!")
